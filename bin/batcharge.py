#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import subprocess


color_green = '%{[32m%}'
color_yellow = '%{[1;33m%}'
color_red = '%{[31m%}'
color_reset = '%{[00m%}'

status = subprocess.Popen(["cat", "/sys/class/power_supply/C23B/status"],
                          stdout=subprocess.PIPE)
status_output = status.communicate()[0]
try:
    charging = "Charging" in status_output
except TypeError:
    charging = status_output.find(b"Charging") > -1

try:
    full = "Full" in status_output
except TypeError:
    full = status_output.find(b"Full") > -1

if charging:
    try:
        sys.stdout.write(color_yellow + u"⚡ ".encode('utf-8') + color_reset)
    except TypeError:
        sys.stdout.write(color_yellow + u"⚡ " + color_reset)
elif full:
    sys.stdout.write('')
else:
    capacity = subprocess.Popen(["cat",
                                 "/sys/class/power_supply/C23B/capacity"],
                                stdout=subprocess.PIPE)
    capacity_output = int(capacity.communicate()[0])
    if capacity_output >= 75:
        out = u'∷'
    elif capacity_output >= 50:
        out = u'∴'
    elif capacity_output >= 25:
        out = u'∶'
    else:
        out = u'⤬'

    color_out = (
        color_reset if capacity_output >= 50
        else color_yellow if capacity_output >= 25
        else color_red
    )

    try:
        out = color_out + out.encode('utf-8') + ' ' + color_reset
    except TypeError:
        out = color_out + out + ' ' + color_reset
    sys.stdout.write(out)
